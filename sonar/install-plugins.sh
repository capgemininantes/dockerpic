#!/bin/bash

install_sonar_plugin(){
	plugin_name=$1
	plugin_version=$2
	url="$SONAR_DOWNLOAD_URL/$plugin_name/$plugin_name-$plugin_version.jar"
	url_backup="$SONAR_DOWNLOAD_BACKUP_URL/$plugin_name/$plugin_version/$plugin_name-$plugin_version.jar"

	if curl --output /dev/null --silent --head --fail "$url"; then
	  echo "Downloading $plugin_name from $url"
	else
	   url=$url_backup;
	   echo "Switch URL! Downloading $plugin_name from $url"
	fi
	cd ${SONARQUBE_HOME}/extensions/plugins/
	curl -o "$plugin_name-$plugin_version.jar" -fSL "$url"
	status="$?"
	return $status
}

# Usage => install_all_sonar_plugins "<plugin1>:<version-plugin1>|<plugin2>:<version-plugin2>"
# Exemple => install_all_sonar_plugins "sonar-scm-git-plugin:1.1|sonar-scm-git-plugin:1.2|sonar-java-plugin:3.56|sonar-scm-svn-plugin:1.3"
install_all_sonar_plugins(){
	list_of_plugins_versions=$1
 	IFS='|' read -ra PLUGINS_VERSIONS <<< "${list_of_plugins_versions}"
	for i in "${PLUGINS_VERSIONS[@]}"; do
	    IFS=':' read -ra PLUGINS <<< "${i}"
		if [ "${#PLUGINS[@]}" -eq "2" ]; 
		then
			echo "Installation of sonar-plugin ${PLUGINS[0]} version ${PLUGINS[1]}"
			install_sonar_plugin ${PLUGINS[0]} ${PLUGINS[1]}
		fi
	done
}

if [ -z "$SONAR_DOWNLOAD_URL" ];
then
	export  SONAR_DOWNLOAD_URL="https://sonarsource.bintray.com/Distribution"
fi

if [ -z "$SONAR_DOWNLOAD_BACKUP_URL" ];
then
	echo "export sonarqube_home"
	export SONAR_DOWNLOAD_BACKUP_URL="https://downloads.sonarsource.com/plugins/org/codehaus/sonar-plugins"
fi

if [ -z "SONARQUBE_HOME" ];
then
	echo "export sonarqube_home"
	export SONARQUBE_HOME = /opt/sonarqube
fi

if [ -z "$1" ];
then
	install_all_sonar_plugins "$SONAR_PLUGINS" 
else
	install_all_sonar_plugins "$1" 
fi
