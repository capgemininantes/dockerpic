## Sonar-Pic Docker Container 


Usage:
```
$ docker build -t sonar-pic .
$ docker run -d -p=9000:9000 sonar-pic
```

```
// use it in compose
ENV SONAR_DOWNLOAD_URL
ENV SONAR_DOWNLOAD_BACKUP_URL
ENV SONAR_PLUGINS

// Usage SONAR_PLUGINS="<plugin1>:<versionOfPlugin1>|<plugin2>:<versionOfPlugin2>"
```
It use an install-plugins.sh script to install the list of SONAR_PLUGINS with given SONAR_DOWNLOAD_URL and SONAR_DOWNLOAD_BACKUP_URL

Once Sonar is up and running go to http://${MACHINE_IP}:9000