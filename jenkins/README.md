## Jenkins-Pic Docker Container

Usage:
```
$ docker build -t jenkins-pic .
$ docker run -d -p=18080:8080 jenkins-pic
```

Once Jenkins is up and running go to http://${MACHINE_IP}:18080

Usage, force use of HTTPS with a certificate included in the image and prefix /jenkins:
```
$ docker build -t jenkins-pic .
$ docker run -d -p=18080:8081 --env JENKINS_OPTS="--prefix=/jenkins --httpPort=-1 --httpsPort=8081 --httpsKeyStore=/var/jenkins_home/.ssl/keystore.jks --httpsKeyStorePassword=$(PASSWORD_KEYSTORE)"  jenkins-pic
```

Once Jenkins is up and running go to https://${MACHINE_IP}:18080/jenkins