#!/bin/bash
set -e

echo "Creating gitlab user and gitlabhq_production database on postgres"
psql -v ON_ERROR_STOP=1 --username "postgres" <<-EOSQL
    CREATE USER gitlab PASSWORD '${GITLAB_PWD}';
    ALTER ROLE gitlab Superuser;
    CREATE ROLE "gitlab-psql" WITH Superuser;

    CREATE DATABASE gitlabhq_production;
    ALTER DATABASE gitlabhq_production OWNER TO gitlab;
    ALTER USER gitlab SET SEARCH_PATH = PUBLIC,EXTENSIONS;
EOSQL

echo "Giving all ownership and right to gitlab user"
psql -v ON_ERROR_STOP=1 --username "postgres" -d gitlabhq_production <<-EOSQL
    ALTER SCHEMA public OWNER TO gitlab;
    GRANT ALL ON ALL TABLES IN SCHEMA public TO gitlab;
    GRANT ALL ON SCHEMA public TO gitlab;
    CREATE EXTENSION pg_trgm;
EOSQL


echo "Creating jenkins user and jenkins database on postgres"
psql -v ON_ERROR_STOP=1 --username "postgres" <<-EOSQL
    CREATE USER jenkins PASSWORD '${JENKINS_PWD}';
    ALTER ROLE jenkins Superuser;
    CREATE ROLE "jenkins-psql" WITH Superuser;

    CREATE DATABASE jenkins;
    ALTER DATABASE jenkins OWNER TO jenkins;
    ALTER USER jenkins SET SEARCH_PATH = PUBLIC,EXTENSIONS;
EOSQL

echo "Giving all ownership and right to jenkins user"
psql -v ON_ERROR_STOP=1 --username "postgres" -d jenkins <<-EOSQL
    ALTER SCHEMA public OWNER TO jenkins;
    GRANT ALL ON ALL TABLES IN SCHEMA public TO jenkins;
    GRANT ALL ON SCHEMA public TO jenkins;
    CREATE EXTENSION pg_trgm;
EOSQL


echo "Creating sonar user and sonar database on postgres"
psql -v ON_ERROR_STOP=1 --username "postgres" <<-EOSQL
    CREATE USER sonar PASSWORD '${SONAR_PWD}';
    ALTER ROLE sonar Superuser;
    CREATE ROLE "sonar-psql" WITH Superuser;

    CREATE DATABASE sonar;
    ALTER DATABASE sonar OWNER TO sonar;
    ALTER USER sonar SET SEARCH_PATH = PUBLIC,EXTENSIONS;
EOSQL

echo "Giving all ownership and right to sonar user"
psql -v ON_ERROR_STOP=1 --username "postgres" -d sonar <<-EOSQL
    ALTER SCHEMA public OWNER TO sonar;
    GRANT ALL ON ALL TABLES IN SCHEMA public TO sonar;
    GRANT ALL ON SCHEMA public TO sonar;
    CREATE EXTENSION pg_trgm;
EOSQL

echo "Importing databases content..."
psql --username "postgres" -d gitlabhq_production -a -f /docker-entrypoint-initdb.d/sql/gitlabhq_production.sql
psql --username "postgres" -d jenkins -a -f /docker-entrypoint-initdb.d/sql/jenkins.sql
psql --username "postgres" -d sonar -a -f /docker-entrypoint-initdb.d/sql/sonar.sql

echo "Replace trust access on remote by password access"
sed -i 's/\(host all all 0.0.0.0\/0 \)trust/\1password/g' /var/lib/postgresql/data/pg_hba.conf
