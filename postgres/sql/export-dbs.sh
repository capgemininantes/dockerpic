#!/bin/bash

EXPORTS="gitlabhq_production sonar jenkins"
LOG_FILE="./exports.log"

EX_OK=0

echo "Starting exports..." | tee -a ${LOG_FILE}
for db in ${EXPORTS} ; do
	echo "	Exporting '${db}'..." | tee -a ${LOG_FILE}
	if [ -f ${db}.sql ] ; then
		echo "		File '${db}.sql' already exists"
		db_save=$(basename ${db}.sql .sql)_$(date "+%Y%M%d").sql
		echo "		Moving at '${db_save}'..."
		mv ${db}.sql ${db_save}
		ret_code=$?
		if [ ${ret_code} -eq 0 ] ; then
			echo "		'${db}' moved at '${db_save}'" | tee -a ${LOG_FILE}
		else
			echo "		'ERROR: can not move '${db}' to '${db_save}'" | tee -a ${LOG_FILE}
		fi
	fi
	pg_dump -d ${db} -U postgres > ${db}.sql
	ret_code=$?
	if [ ${ret_code} -eq 0 ] ; then
		echo "	'${db}' exported successfully" | tee -a ${LOG_FILE}
	else
		echo "	'ERROR: '${db}' could not be exported" | tee -a ${LOG_FILE}
		EX_OK=$((${EX_OK} + ${ret_code}))
	fi
done
if [ ${EX_OK} -eq 0 ] ; then
	echo "Exported done" | tee -a ${LOG_FILE}
else
	echo "WARNING: There were some export errors see log file '${LOG_FILE}'" | tee -a ${LOG_FILE}
fi
exit ${EX_OK}
