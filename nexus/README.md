## Nexus-Pic SSL Docker Container 

Redirection http to https is enabled.
SSL AutoSigned certificate and keystore.


Usage:
```
$ docker build -t nexus-pic .
$ docker run -d -p=18080:8081 -p=18443:8443 nexus-pic
```

Once Nexus is up and running go to http://${MACHINE_IP}:18081 => http://${MACHINE_IP}:18443

--build-arg
```
ARG SSL_STOREPASS=changeit
ARG SSL_MANAGERPASS=changeit
ARG SSL_KEYPASS=changeit
```
Key password must be at least 6 characters


--env
```
// Use it in compose
ENV SONATYPE_WORK /opt/nexus/
ENV NEXUS_DATA /nexus-data
ENV NEXUS_VERSION 3.0.0-03
```


```
$ docker build --build-arg SSL_STOREPASS=123456 --build-arg SSL_MANAGERPASS=123456 --build-arg SSL_KEYPASS=123456  -t nexus-pic .
$ docker run -d -p=18080:8081 -p=18443:8443 nexus-pic
```

Once Nexus is up and running go to http://${MACHINE_IP}:18081 => http://${MACHINE_IP}:18443